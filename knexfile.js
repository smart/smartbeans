// Update with your config settings.

export const development = {
  client: 'sqlite3',
  connection: {
    filename: "./DummyDB.sqlite"
  },
  migrations: {
    tableName: 'knex_migrations'
  }
};

export const production = {
  client: 'postgres',
  connection: {
    host: "localhost",
    port: 5432,
    database: 'smartbeans',
    user: 'smartbeans',
    password: 'smartbeans'
  },
  pool: {
    min: 2,
    max: 30
  },
  migrations: {
    tableName: 'knex_migrations'
  }
};
