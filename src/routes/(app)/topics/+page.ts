import type { PageLoad } from './$types';

export const load: PageLoad = async ({ fetch, parent }) => {
	const data = await parent();
	const tasksRequest = await fetch(`/api/courses/${data.course.name}/tasks`);
	const tasks = await tasksRequest.json();

	const progressRequest = await fetch(`/api/courses/${data.course.name}/progress`, {
		headers: { 'cache-control': 'no-cache', pragma: 'no-cache' }
	});
	const progress = await progressRequest.json();

	return {
		levels: data.course.config.tasks.topicsView.levels,
		progress: progress,
		tasks: tasks
	};
};
