import type { Topic } from '$lib/utils/types/topic';
import type { PageLoad } from './$types';

export const load: PageLoad = async ({ fetch, params, parent }) => {
	const data = await parent();
	const tasksRequest = await fetch(`/api/courses/${data.course.name}/tasks`);
	const tasks = await tasksRequest.json();

	const progressRequest = await fetch(`/api/courses/${data.course.name}/progress`, {
		headers: { 'cache-control': 'no-cache', pragma: 'no-cache' }
	});
	const progress = await progressRequest.json();

	const filterTopic = data.course.config.tasks.topicsView.levels
		.flat()
		.filter((topic: Topic) => topic.name === params.topic)[0];

	return {
		progress: progress,
		tasks: tasks.filter((task: { tags: any[] }) =>
			task.tags
				.map((tag: { name: string }) => tag.name.toLocaleLowerCase())
				.includes(params.topic.toLocaleLowerCase())
		),
		course: data.course,
		filterTopic: filterTopic
	};
};
