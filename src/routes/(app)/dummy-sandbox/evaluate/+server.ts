/**
 * ### POST `/dummy-sandbox/evaluate`
 *
 * Emulates a submission to the sandbox . Can be used instead of the real
 * sandbox for testing purposes. The Input and Output formats are equivalent to
 * the sandbox. If the submission text includes the string "please", the
 * submission will be accepted, otherwise it will be denied. Always returns 200.
 *
 * ***Input***
 * ```
 * {
 *   "submission": <String>
 * }
 * ```
 *
 * ***Output***
 * ```
 * {
 *  "type": <String>,
 *  "simplified": {
 *    "compiler": {
 *      "stdout": <String>,
 *      "exitCode:" <Integer>
 *    }
 *  },
 *  "score": <Integer>
 * }
 * ```
 *
 */

import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const POST: RequestHandler = async ({ request, locals, params }) => {
	const body = await request.json();
	let result;
	if (body.submission.includes('please', 0)) {
		result = {
			type: 'SUCCESS',
			simplified: {
				compiler: {
					stdout: 'Your wish has been granted!',
					exitCode: 0
				}
			},
			score: 1
		};
	} else {
		result = {
			type: 'COMPILE_ERROR',
			simplified: {
				compiler: {
					stdout:
						'If you want this task to be accepted, you have to ask nicely ' +
						'(include "please" in your submission)',
					exitCode: 1
				}
			},
			score: 0
		};
	}

	return json(result);
};
