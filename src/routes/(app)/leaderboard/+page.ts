import type { PageLoad } from './$types';

export const load: PageLoad = async ({ fetch, parent }) => {
	const data = await parent();
	const request = await fetch(`/api/courses/${data.course.name}/leaderboard`);
	const leaders = await request.json();
	return {
		leaders: leaders
	};
};
