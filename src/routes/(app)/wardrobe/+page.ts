import type { PageLoad } from './$types';

export const load: PageLoad = async ({ fetch }) => {
	const requestAssets = await fetch('/api/user/assets');
	const assets = await requestAssets.json();
	return { assets: assets };
};
