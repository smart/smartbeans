import type { PageLoad } from './$types';

export const load: PageLoad = async ({ parent, fetch }) => {
	const data = await parent();
	const requestLeaders = await fetch(`/api/courses/${data.course.name}/leaderboard`);
	const leaders = await requestLeaders.json();
	const requestProgress = await fetch(`/api/courses/${data.course.name}/progress`);
	const progress = await requestProgress.json();
	const requestTasks = await fetch(`/api/courses/${data.course.name}/tasks`);
	const tasks = await requestTasks.json();

	return {
		leaders: leaders,
		progress: progress,
		tasks: tasks
	};
};
