import type { PageLoad } from './$types';

export const load: PageLoad = async ({ parent, url, fetch }) => {
	const data = await parent();

	const tags = [...url.searchParams.keys()];

	const tasksRequest = await fetch(`/api/courses/${data.course.name}/tasks`);
	const tasks = await tasksRequest.json();

	const progressRequest = await fetch(`/api/courses/${data.course.name}/progress`, {
		headers: { 'cache-control': 'no-cache', pragma: 'no-cache' }
	});
	const progress = await progressRequest.json();

	return { tasks: tasks, progress: progress, course: data.course, tags: tags };
};
