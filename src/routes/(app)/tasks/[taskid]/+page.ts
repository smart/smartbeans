import type { PageLoad } from './$types';

export const load: PageLoad = async ({ fetch, params, parent }) => {
	const data = await parent();

	const taskRequest = await fetch(`/api/courses/${data.course.name}/tasks/${params.taskid}`);

	if (!taskRequest.ok) return { status: taskRequest.status, error: await taskRequest.text() };
	const task = await taskRequest.json();

	const submissionsRequest = await fetch(
		`/api/courses/${data.course.name}/tasks/${params.taskid}/submissions`
	);

	const submissions = await submissionsRequest.json();
	return { task: task, submissions: submissions, course: data.course };
};
