import { error, redirect } from '@sveltejs/kit';
import type { LayoutServerLoad } from './$types';

export const load: LayoutServerLoad = async ({ locals }) => {
	if (!locals.user) {
		throw redirect(302, '/auth/login');
	}
	if (!locals.course) {
		throw error(400, `Course '${locals.user.activeCourse}' is not defined!`);
	}
	return { course: locals.course, user: locals.user };
};
