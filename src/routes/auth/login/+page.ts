import type { PageLoad } from './$types';

export const load: PageLoad = async ({ fetch }) => {
	const response = await fetch('/api/auth/meta');
	if (!response.ok) {
		return { status: response.status };
	}

	const courses: { name: string }[] = await response.json();

	return { courses: courses.map((c: { name: string }) => c.name) };
};
