import database from '$lib/database/database';
import { schemaAsset, type Asset } from '$lib/utils/types/assets';
import { error } from '@sveltejs/kit';
import * as yup from 'yup';
import type { RequestHandler } from './$types';

export const POST: RequestHandler = async ({ request }) => {
	if (request.headers.get('Content-Type') !== 'application/json')
		throw error(400, 'Request must be content-type of application/json');

	const contentLength = request.headers.get('Content-Length');
	if (contentLength && parseInt(contentLength) === 0)
		throw error(400, 'Request body must be defined');

	let body;
	try {
		body = await request.json();
	} catch {
		throw error(400, 'Request body must be a json object');
	}

	let asset: Asset;
	// Validate body data
	try {
		asset = await schemaAsset.validate(body, { stripUnknown: true });
	} catch (e) {
		throw error(400, e instanceof yup.ValidationError ? JSON.stringify(e.errors) : 'Unknown error');
	}

	// Check asset already in database then update otherwise insert
	const query = await database.table('assets').where('id', asset.id).first();
	if (query !== undefined) {
		await database.table('assets').update(asset).where('id', asset.id);
	} else {
		await database.table('assets').insert(asset);
	}

	return new Response();
};
