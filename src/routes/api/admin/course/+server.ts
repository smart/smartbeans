import database from '$lib/database/database';
import { schemaCourse, type Course } from '$lib/utils/types/course';
import { error } from '@sveltejs/kit';
import * as yup from 'yup';
import type { RequestHandler } from './$types';

export const POST: RequestHandler = async ({ request }) => {
	const body = await request.json();
	let course: Course;

	// Validate body data
	try {
		course = await schemaCourse.validate(body, { stripUnknown: true });
	} catch (e) {
		throw error(400, e instanceof yup.ValidationError ? JSON.stringify(e.errors) : 'Unknown Error');
	}

	try {
		// Insert course into database
		await database.table('courses').insert(course);
	} catch {
		// Update course into database when not existing
		await database.table('courses').update(course).where('name', course.name);
	}

	return new Response();
};
