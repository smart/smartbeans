import database from '$lib/database/database';
import { passwordHash, passwordVerify } from '$lib/utils/authentication/password';
import { error } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const DELETE: RequestHandler = async ({ locals, request }) => {
	const body = await request.json();

	if (body.password === undefined) {
		throw error(400, 'Password is not defined in body.');
	}
	if (!locals.user.ltiEnabled) {
		throw error(400, 'LTI is disabled. The password cannot be deleted!');
	}

	const hash = await database
		.table('users')
		.select('password')
		.where('username', locals.user.username)
		.first();

	if (!hash || hash.password === null) {
		throw error(400, 'No password set for this user!');
	}

	if (!(await passwordVerify(hash.password, body.password))) {
		throw error(401, 'Wrong password!');
	}
	await database.table('users').update('password', null).where('username', locals.user.username);
	return new Response();
};

export const POST: RequestHandler = async ({ locals, request }) => {
	const body = await request.json();

	if (body.password === undefined) {
		throw error(400, 'Password is not defined in body.');
	}

	if (locals.user.passwordSet) {
		throw error(401, 'Use the patch route for changing password.');
	}

	await database
		.table('users')
		.update('password', await passwordHash(body.password))
		.where('username', locals.user.username);
	return new Response();
};

export const PATCH: RequestHandler = async ({ request, locals }) => {
	const body = await request.json();

	if (body.password === undefined) {
		throw error(400, 'Password is not defined in body.');
	}
	if (body.oldPassword === undefined) {
		throw error(400, 'Old password is not defined in body.');
	}

	if (!locals.user.passwordSet) {
		throw error(401, 'Use the post route for creating a password.');
	}

	const hash = await database
		.table('users')
		.select('password')
		.where('username', locals.user.username)
		.first();

	if (!hash || !(await passwordVerify(hash.password, body.oldPassword))) {
		throw error(401, 'Wrong old password!');
	}

	await database
		.table('users')
		.update('password', await passwordHash(body.password))
		.where('username', locals.user.username);
	return new Response();
};
