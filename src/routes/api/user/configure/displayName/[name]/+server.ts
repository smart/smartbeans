import database from '$lib/database/database';
import { error } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const POST: RequestHandler = async ({ locals, params }) => {
	if (params.name.length < 3 || params.name.length > 16) {
		throw error(400, 'Display name length must be between 3 and 16 characters!');
	}
	await database
		.table('users')
		.update({ displayName: params.name })
		.where('username', locals.user.username);

	return new Response();
};
