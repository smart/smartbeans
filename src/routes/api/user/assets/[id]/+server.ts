import database from '$lib/database/database';
import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

// Returns the asset data for the requested asset id
export const GET: RequestHandler = async ({ locals, params }) => {
	const assets = await database.table('assets').select('*').where('id', params.id);

	const unlocked: string[] = (
		await database
			.table('unlockedAssets')
			.where('user', locals.user.username)
			.andWhere('assetId', params.id)
			.select('assetId')
	).map((x) => x.assetId);

	const prepared = assets.map((asset) => {
		return {
			...asset,
			unlocked: unlocked.includes(asset.id)
		};
	});

	prepared.sort((a, b) => (a.unlocked ? -1 : 1));

	return json(prepared);
};
