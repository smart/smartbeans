import database from '$lib/database/database';
import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ locals }) => {
	const assets = await database.table('assets').select('*');

	const unlocked: string[] = (
		await database.table('unlockedAssets').where('user', locals.user.username).select('assetId')
	).map((x) => x.assetId);

	const prepared = assets.map((asset) => {
		return {
			...asset,
			unlocked: unlocked.includes(asset.id)
		};
	});

	prepared.sort((a, b) => (a.unlocked ? -1 : 1));

	return json(prepared);
};
