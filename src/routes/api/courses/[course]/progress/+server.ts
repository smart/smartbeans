/**
 * ### GET `/courses/<course>/progress`
 *
 * Returns all solved tasks as a list of taskids. Requires header `Authorization: Bearer <valid (session) token>`.
 *
 * ***Output***
 * ```
 * [<Integer>, ...]
 * ```
 *
 * ***Errors***
 * - 400: Invalid header format
 * - 401: Invalid token
 * - 403: Course does not correspond with session course
 */
import database from '$lib/database/database';
import { error, json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ locals, params }) => {
	const user = locals.user;
	if (user.activeCourse !== params.course) {
		throw error(403, 'Course does not correspond with session course');
	}

	let progress = (
		await database
			.table('submissions')
			.join('courseTask', 'submissions.taskid', 'courseTask.taskid')
			.where('user', user.username)
			.andWhere('courseTask.course', params.course)
			.andWhere('score', '>=', 0.9999)
			.select('submissions.taskid')
	).map((x) => x.taskid);
	progress = [...new Set(progress)];
	return json(progress);
};
