/**
 * ### GET `/courses/<course>/tasks`
 *
 * Returns a list of all tasks for the given course.
 *
 * ***Output***
 * ```
 * [
 *   {
 *     "taskid": <Integer>,
 *     "taskDescription": <String>,
 *     "lang": <String>,
 *     "tags": <String>,
 *     "orderBy": <Integer>,
 *     "prerequisites": <String>
 *   },
 *   ...
 * ]
 * ```
 *
 * ***Errors***
 * - 404: Invalid course
 */
import database from '$lib/database/database';
import { serverRequestCache } from '$lib/utils/cache';
import { error, json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ params }) => {
	const courseAvailable =
		(await database('courses').select('name').where({ name: params.course })).length > 0;
	if (!courseAvailable) {
		throw error(404, 'Invalid course');
	}

	const tasks = await serverRequestCache(
		`tasks-${params.course}`,
		600000 /* 10min caching */,
		async () => {
			const data = await database
				.table('tasks')
				.join('courseTask', 'tasks.taskid', 'courseTask.taskid')
				.select(
					'tasks.taskid',
					'tasks.taskDescription',
					'tasks.solution',
					'tasks.lang',
					'tasks.tests',
					'courseTask.tags',
					'courseTask.orderBy',
					'courseTask.prerequisites'
				)
				.where({ course: params.course });
			return data.map((d) => ({
				...d,
				taskDescription:
					typeof d.taskDescription === 'string' ? JSON.parse(d.taskDescription) : d.taskDescription,
				tags: typeof d.tags === 'string' ? JSON.parse(d.tags) : d.tags,
				prerequisites:
					typeof d.prerequisites === 'string' ? JSON.parse(d.prerequisites) : d.prerequisites
			}));
		}
	);

	return json(tasks);
};
