/**
 * ### GET `/courses/<course>/tasks/<taskid>`
 *
 * Returns a single task.
 *
 * ***Output***
 * ```
 * {
 *   "taskid": number,
 *   "task_description": string,
 *   "lang": string,
 *   "tags": [ { name: string, points?: number } ],
 *   "order_by": number,
 *   "prerequisites": number[],
 *   "unlockableAssets": number[]
 * }
 * ```
 *
 * ***Errors***
 * - 404: Invalid course or task id
 * - 500: Multipe tasks with same taskid
 *
 */
import database from '$lib/database/database';
import { serverRequestCache } from '$lib/utils/cache';
import { error, json } from '@sveltejs/kit';
import * as yup from 'yup';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ params }) => {
	if (!yup.number().isValidSync(params.taskid)) {
		throw error(400, 'Taskid needs to be an integer');
	}

	let task = await serverRequestCache(
		`task-${params.course}-${params.taskid}`,
		600000 /* 10min caching */,
		async () => {
			const data = await database
				.table('tasks')
				.leftJoin('courseTask', 'tasks.taskid', 'courseTask.taskid')
				.select(
					'tasks.taskid',
					'tasks.taskDescription',
					'tasks.lang',
					'courseTask.tags',
					'courseTask.orderBy',
					'courseTask.prerequisites',
					'courseTask.unlockableAssets'
				)
				.where('tasks.taskid', params.taskid)
				.andWhere('course', params.course);
			return data.map((d) => ({
				...d,
				taskDescription:
					typeof d.taskDescription === 'string' ? JSON.parse(d.taskDescription) : d.taskDescription,
				tags: typeof d.tags === 'string' ? JSON.parse(d.tags) : d.tags,
				prerequisites:
					typeof d.prerequisites === 'string' ? JSON.parse(d.prerequisites) : d.prerequisites,
				unlockableAssets:
					typeof d.unlockableAssets === 'string'
						? JSON.parse(d.unlockableAssets)
						: d.unlockableAssets
			}));
		}
	);

	if (task.length === 0) {
		throw error(404, 'Invalid course or taskid');
	}
	if (task.length > 1) {
		throw error(500, 'Multipe tasks with same taskid');
	}
	task = task[0];

	return json(task);
};
