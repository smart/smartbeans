import database from '$lib/database/database';
import { json } from '@sveltejs/kit';
import type { Knex } from 'knex';
import type { StringSchema } from 'yup';
import type { AssertsShape, Assign, ObjectShape } from 'yup/lib/object';
import type { RequiredStringSchema } from 'yup/lib/string';
import type { AnyObject } from 'yup/lib/types';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ locals, params }) => {
	let { unlockableAssets }: any = await database
		.table('courseTask')
		.select('unlockableAssets')
		.where('taskid', params.taskid)
		.first();
	unlockableAssets =
		typeof unlockableAssets === 'string' ? JSON.parse(unlockableAssets) : unlockableAssets;

	const assets: AssertsShape<
		Assign<
			ObjectShape,
			{
				id: RequiredStringSchema<string | undefined, AnyObject>;
				outfitId: RequiredStringSchema<string | undefined, AnyObject>;
				type: StringSchema<string | undefined, AnyObject, string | undefined>;
				name: RequiredStringSchema<string | undefined, AnyObject>;
				details: any;
			}
		>
	>[] = [];
	unlockableAssets.forEach(async (assetId: Knex.DbColumn<string> | null) => {
		const asset = await database.table('assets').select('*').where('id', assetId).first();
		if (asset) assets.push(asset);
	});

	const unlocked: string[] = (
		await database.table('unlockedAssets').where('user', locals.user.username).select('assetId')
	).map((x) => x.assetId);

	const prepared = assets.map((asset) => {
		return {
			...asset,
			unlocked: unlocked.includes(asset.id)
		};
	});

	prepared.sort((a, b) => (a.unlocked ? -1 : 1));

	return json(prepared);
};
