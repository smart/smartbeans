/**
 * ### GET `/courses/<course>/tasks/<taskid>/submissions`
 *
 * Returns the submissions for the user in this course and a given task. Requires header `Authorization: Bearer <valid (session) token>`. Returns an empty array if the taskid is invalid.
 *
 * ***Output***
 * ```
 * [
 *   {
 *     "id": <Integer>,
 *     "taskid": <Integer>,
 *     "timestamp": <Integer>,
 *     "content": <String>,
 *     "result_type" <String>,
 *     "simplified": {
 *       "compiler": {
 *         "stdout": <String>,
 *         "statusCode": <Integer>
 *       },
 *       "testCase": {
 *         "stdin": <String>,
 *         "stdout": <String>,
 *         "expectedStdout": <String>,
 *         "statusCode": <Integer>
 *       }
 *     },
 *     "score": <Float>
 *   },
 *   ...
 * ]
 * ```
 *
 * ***Errors***
 * - 400: Invalid header format
 * - 401: Invalid token
 * - 403: Course does not correspond with session course
 *
 *
 * ### POST `/courses/<course>/tasks/<taskid>/submissions`
 *
 * Submits a solution to the sandbox. Returns 200 on success. Requires header `Authorization: Bearer <valid (session) token>`.
 *
 * ***Input***
 * ```
 * {
 *   "submission": <String>
 * }
 * ```
 *
 * ***Output***
 * ```
 * -
 * ```
 *
 * ***Errors***
 * - 400: Invalid header or body format
 * - 401: Invalid token
 * - 403: Course does not correspond with session course
 */
import { SANDBOX_URL } from '$env/static/private';
import database from '$lib/database/database';
import { schemaAssetUnlocked } from '$lib/utils/types/assets';
import { error, json } from '@sveltejs/kit';
import type { Knex } from 'knex';
import type { RequestHandler } from './$types';

const codewords = [
	"Abenteuer",
	"Ananas",
	"Bienenstock",
	"Blumenstrauss",
	"Buchstaben",
	"Chamäleon",
	"Diamant",
	"Eichhörnchen",
	"Elefant",
	"Erdbeerkuchen",
	"Fahrrad",
	"Fischernetz",
	"Flugzeug",
	"Frosch",
	"Giraffe",
	"Glückspilz",
	"Goldfisch",
	"Grashüpfer",
	"Handschuhe",
	"Hauskatze",
	"Herzschlag",
	"Hubschrauber",
	"Kaktus",
	"Kamille",
	"Karamell",
	"Kartoffel",
	"Kirschkuchen",
	"Kolibri",
	"Krokodil",
	"Lampion",
	"Lavendel",
	"Leopard",
	"Löwenmähne",
	"Luftballon",
	"Magnet",
	"Märchen",
	"Marmelade",
	"Mauseloch",
	"Meerjungfrau",
	"Mondlicht",
	"Nebelhorn",
	"Oktopus",
	"Papagei",
	"Pfefferminze",
	"Pinguin",
	"Piratenschiff",
	"Regenbogen",
	"Reiseführer",
	"Ritterrüstung",
	"Rosenkranz",
	"Schneemann",
	"Schmetterling",
	"Schneewittchen",
	"Sonnenschein",
	"Spielzeug",
	"Sternschnuppe",
	"Streichholz",
	"Teekanne",
	"Tintenfisch",
	"Tropeninsel",
	"Turmzimmer",
	"Walross",
	"Wassermelone",
	"Wunderland",
	"Zauberstab",
	"Zirkuszelt",
	"Zitronensorbet",
	"Zuckerwatte",
	"Zwergenmütze",
	"Astronaut",
	"Baumhaus",
	"Bumerang",
	"Bücherregal",
	"Einhorn",
	"Eisbär",
	"Fabelwesen",
	"Fensterladen",
	"Fledermaus",
	"Frühlingsblume",
	"Gartenzwerg",
	"Geheimnis",
	"Glühwürmchen",
	"Himmelsleiter",
	"Kerzenlicht",
	"Kleeblatt",
	"Märchenschloss",
	"Mitternacht",
	"Märchenwald",
	"Nachtigall",
	"Nordlicht",
	"Pusteblume",
	"Regenwolke",
	"Rosengarten",
	"Schatzkarte",
	"Schlüsselloch",
	"Sommerbrise",
	"Sternenstaub",
	"Wolkenkratzer",
	"Anker",
	"Bambus",
	"Bär",
	"Berg",
	"Bogen",
	"Buch",
	"Delfin",
	"Drache",
	"Eis",
	"Feder",
	"Feuer",
	"Fuchs",
	"Glück",
	"Hafen",
	"Hut",
	"Kirschblüte",
	"Kreuz",
	"Kuss",
	"Laterne",
	"Leiter",
	"Mond",
	"Musik",
	"Nest",
	"Orchidee",
	"Panda",
	"Perle",
	"Pferd",
	"Pilz",
	"Platz",
	"Puppe",
	"Rabe",
	"Regen",
	"Rose",
	"Schildkröte",
	"Schmetterling",
	"Schnee",
	"See",
	"Sonne",
	"Spiegel",
	"Sturm",
	"Tiger",
	"Tür",
	"Vogel",
	"Wald",
	"Welle",
	"Wolke",
	"Wunder",
	"Zaun"
  ];

export const GET: RequestHandler = async ({ locals, params }) => {
	const user = locals.user;
	if (user?.activeCourse !== params.course) {
		throw error(403, 'Course does not correspond with session course');
	}

	const submissions = await database('submissions')
		.join('courseTask', 'submissions.taskid', 'courseTask.taskid')
		.where('submissions.taskid', params.taskid)
		.andWhere('user', user.username)
		.andWhere('courseTask.course', params.course)
		.select(
			'submissions.id',
			'submissions.course',
			'submissions.taskid',
			'timestamp',
			'content',
			'resultType',
			'simplified',
			'details',
			'score'
		);

	return json(submissions);
};

const asyncFilter = async (array: Array<any>, predicate: any) => {
	const results = await Promise.all(array.map(predicate));

	return array.filter((_v: any, index: string | number) => results[index]);
};

// Unlocks assets for the task with `taskid` for the user
const unlockAssets = async (taskid: number, user: string) => {
	let { unlockableAssets } = await database
		.table('courseTask')
		.where('taskid', taskid)
		.select('unlockableAssets')
		.first();
	unlockableAssets =
		typeof unlockableAssets === 'string' ? JSON.parse(unlockableAssets) : unlockableAssets;
	let toUnlock = await asyncFilter(
		unlockableAssets,
		async (asset: Knex.DbColumn<string> | null) => {
			return (
				(
					await database
						.table('unlockedAssets')
						.where('user', user)
						.andWhere('assetId', asset)
						.select('*')
				).length === 0
			);
		}
	);

	// Async map
	toUnlock = await Promise.all(
		toUnlock.map(async (asset) => {
			const row = schemaAssetUnlocked.validateSync({ user: user, assetId: asset });
			await database.table('unlockedAssets').insert(row);
			return asset;
		})
	);
	return toUnlock;
};

export const POST: RequestHandler = async ({ request, locals, params }) => {
	const user = locals.user;
	const taskid = parseInt(params.taskid);
	const body = await request.json();
	const submission = body.submission;

	const course = await database.table('courses').where('name', params.course).first();
	if (!course) {
		throw error(404, `Course ${course} not found!`);
	}

	const task = await database.table('tasks').where('taskid', taskid).first();
	if (!task) {
		throw error(404, `Task ${taskid} not found!`);
	}
		  
	// handle codewords
	console.error(JSON.stringify(task.tests));
	if(task.tests.length > 0 && task.tests[0].testtype === "codeword") {
		const len = user.username.length;
		let index = 0;
		if(len > 1)
			index = Math.abs(((user.username.charCodeAt(len - 1) + user.username.charCodeAt(len - 2))) + taskid);
		console.error(index);
		console.error(submission, "===", codewords[index % codewords.length]);
		const correct = submission.trim() === codewords[index % codewords.length];
		const submissionResult = {
			user: user.username,
			course: user.activeCourse,
			taskid: taskid,
			timestamp: new Date().getTime(),
			content: submission,
			resultType: correct ? "SUCCESS" : "WRONG_ANSWER",
			simplified: {message: correct ? "SUCCESS" : "WRONG_ANSWER" },
			details: {},
			score: correct ? 1 : 0
		}
		await database('submissions').insert(submissionResult);
		const newUnlockedAssets = correct ? await unlockAssets(taskid, user.username) : [];
		return json({ result: submissionResult, newUnlockedAssets: newUnlockedAssets });
	}

	const submissionResponse = await fetch(`${SANDBOX_URL}/evaluate`, {
		method: 'POST',
		body: JSON.stringify({
			taskid: taskid,
			lang: task.lang,
			submission: submission,
			tests: task.tests
		}),
		headers: { 'Content-Type': 'application/json' }
	});
	let result;

	const contentLength = submissionResponse.headers.get('content-length');
	if (contentLength && parseInt(contentLength) > (1024*1024)) {
		result = {
			type: 'RUN_ERROR',
			score: 0,
			details: {
				error:
					'Die Sandbox hat einen zu großen Output generiert. Möglicherweise handelt es sich um eine Endlosschleife in der Abgabe.'
			},
			simplified: {
				compiler: {
					stdout: '',
					exitCode: 0
				},
				testCase: {
					stdout: undefined,
					expectedStdout: undefined,
					exitCode: 1
				}
			}
		};
	} else {
		result = await submissionResponse.json();
	}
	const submissionResult = {
		user: user.username,
		course: user.activeCourse,
		taskid: taskid,
		timestamp: new Date().getTime(),
		content: submission,
		resultType: result.type,
		simplified: result.simplified,
		details: result.details ? result.details : {},
		score: result.score
	};

	let newUnlockedAssets = [];
	if (result.score > 0.99999)
		newUnlockedAssets = await unlockAssets(taskid, user.username);

	await database('submissions').insert(submissionResult);

	return json({ result: submissionResult, newUnlockedAssets: newUnlockedAssets });
};
