/**
 * ### GET `/courses/<course>/meta`
 *
 * ***Output***
 * ```
 * {
 *   "name": <String>,
 *   "title": <String>,
 *   "config": {
 *     ...
 *   }
 * }
 * ```
 *
 * ***Errors***
 * - 404: Invalid course
 *
 */
import database from '$lib/database/database';
import { error, json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ params }) => {
	const course = await database.table('courses').where({ name: params.course });
	if (course.length === 0) {
		throw error(404, 'Invalid course');
	}
	return json(course);
};
