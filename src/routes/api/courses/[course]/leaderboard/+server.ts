import database from '$lib/database/database';
import type { Tag } from '$lib/utils/types/tag';
import type { Avatar } from '$lib/utils/types/avatar';
import { serverRequestCache } from '$lib/utils/cache';
import type { RequestHandler } from './$types';
import { json } from '@sveltejs/kit';
import type { string } from 'yup';
import { LEADERBOARD_USERS_LAST_SUBMISSION_TIMEOUT } from '$env/static/private';

const calcLeaderboard = async (course: string) => {
	type CorrectSubmission = {
		tags: Tag[];
		taskid: number;
		username: string;
		displayName: string;
		timestamp: number;
		avatar: Avatar;
	};
	const allCorrectSubmissions: CorrectSubmission[] = await database
		.table('submissions')
		.join('courseTask', 'submissions.taskid', 'courseTask.taskid')
		.where('courseTask.course', course)
		.andWhere('score', '>=', 0.9999)
		.join('users', 'username', 'user')
		.select('tags', 'submissions.taskid', 'username', 'displayName', 'timestamp', 'avatar');

	allCorrectSubmissions.forEach(
		(submission) =>
			(submission.tags =
				typeof submission.tags === 'string' ? JSON.parse(submission.tags) : submission.tags)
	);
	const users = allCorrectSubmissions.reduce((prev: any, curr) => {
		if (!prev[curr.username]) {
			prev[curr.username] = {};
		}
		if (prev[curr.username][curr.taskid] === undefined) {
			prev[curr.username][curr.taskid] = curr;
		} else if (prev[curr.username][curr.taskid].timestamp < curr.timestamp) {
			prev[curr.username][curr.taskid] = curr;
		}
		return prev;
	}, {});
	const data = Object.entries(users)
		.map(([key, value]) => {
			const submissions: CorrectSubmission[] = Object.values(value);
			const points = submissions
				.map((submission: CorrectSubmission) =>
					submission.tags.filter((tag: Tag) => tag.points).map((tag) => tag.points)
				)
				.flat()
				.reduce((a, b) => (a ?? 0) + (b ?? 0));
			const lastSubmissionTimestamp = Math.max(
				...submissions.map((submission: CorrectSubmission) => submission.timestamp)
			);
			const avatar = submissions[0].avatar;
			return {
				user: key,
				name: submissions[0].displayName,
				points: points,
				avatar: avatar,
				lastAt: lastSubmissionTimestamp
			};
		})
		.filter((x) => Date.now() - parseInt(LEADERBOARD_USERS_LAST_SUBMISSION_TIMEOUT) < x.lastAt);

	data.sort((a, b) => (b.points ?? 0) - (a.points ?? 0));
	return data;
};

export const GET: RequestHandler = async ({ params, locals }) => {
	const course = params.course;
	const data = serverRequestCache(
		`leaderboard-${course}`,
		60000 /* 1min caching */,
		async () => await calcLeaderboard(course)
	);

	const user = locals.user;
	let leaderboard = await data;
	leaderboard = leaderboard.map(
		(leader: { name: string; points: number; avatar: Avatar; user: string }) => {
			return {
				name: leader.name,
				points: leader.points,
				avatar: leader.avatar,
				me: leader.user === user.username
			};
		}
	);

	return json(leaderboard);
};
