import { getCourseMappingRow, getUserRow } from '$lib/database/queries';
import { isValidLTIRequest, schemaLTI, type LTIBody } from '$lib/utils/authentication/lti';
import { error, redirect } from '@sveltejs/kit';
import { createUser } from '$lib/utils/authentication/register';
import { createSession } from '$lib/session';
import { LTI_CONSUMER_SECRET } from '$env/static/private';
import type { RequestHandler } from './$types';

/**
 * ### POST `/auth/login/lti`
 *
 * LTI login.
 *
 * ***Input***
 * ```
 * LTI request
 * ```
 *
 * ***Output***
 * ```
 * -
 * ```
 *
 * ***Errors***
 * - 400: Invalid header or body format
 * - 401: Invalid token
 */
export const POST: RequestHandler = async ({ request, cookies, url }) => {
	const requestBody = await request.formData();
	const jsonBody = {};
	requestBody.forEach(function (value, key) {
		jsonBody[key] = value;
	});

	if (!schemaLTI.isValidSync(jsonBody)) {
		throw error(400, 'Invalid LTI body');
	}
	const body: LTIBody = await schemaLTI.validate(jsonBody);

	if (!isValidLTIRequest(body, LTI_CONSUMER_SECRET, url.href)) {
		throw error(400, 'Invalid LTI request');
	}

	const user = await getUserRow(body.lis_person_sourcedid);
	let username: string;

	if (!user) {
		username = body.lis_person_sourcedid;
		await createUser(username, undefined, body.lis_person_name_given);
	} else {
		username = user.username;
	}

	const studipId = body.context_id;
	const course = await getCourseMappingRow(studipId, jsonBody['oauth_consumer_key']);
	if (!course) {
		throw error(400, `Course ${studipId} not defined!`);
	}
	const token = await createSession(username, course.courseName);

	cookies.set('token', `${token};`, { path: '/', secure: true, sameSite: 'lax' });

	throw redirect(302, '/');
};
