/**
 * ### POST `/api/auth/login`
 *
 * Password login.
 *
 * ***Input***
 * ```
 * {
 *   "username": <String>,
 *   "password": <String>,
 *   "course": <String>
 * }
 * ```
 *
 * ***Output***
 * ```
 * {
 *   "token": <String>
 * }
 * ```
 *
 * ***Errors***
 * - 400: Wrong input data
 * - 401: Wrong password
 * - 403: No password set for this user
 * - 404: Non-existing user or course
 */
import { courseExists, createSession } from '$lib/session';
import { getUserRow } from '$lib/database/queries';
import { passwordVerify } from '$lib/utils/authentication/password';
import type { UserRow } from '$lib/utils/types/user';
import type { RequestHandler } from './$types';
import { error, json } from '@sveltejs/kit';

export const POST: RequestHandler = async ({ request, cookies }) => {
	const userCredentials = await request.json();
	const password = userCredentials.password;
	const username = userCredentials.username;
	const course = userCredentials.course;

	if (!username || !password || !course) {
		throw error(400, 'Wrong input data');
	}

	let user: UserRow;
	try {
		user = await getUserRow(username);
	} catch (err) {
		throw error(500, 'Database error');
	}
	if (!user) {
		throw error(404, 'User not found!');
	}
	try {
		if (user.password === null) {
			throw error(403, 'No password set for this user');
		}
		if (await passwordVerify(user.password, password)) {
			let token: string;
			if (await courseExists(course)) {
				try {
					token = await createSession(username, course);
				} catch (err: any) {
					throw error(500, err.message);
				}
				cookies.set('token', token, { path: '/', secure: true, sameSite: 'lax' });
				return json({ token: token });
			} else {
				throw error(404, 'Course not found!');
			}
		} else {
			throw error(401, 'Wrong password');
		}
	} catch (err) {
		throw error(500, 'Error during password hashing.');
	}
};
