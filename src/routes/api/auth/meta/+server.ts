import database from '$lib/database/database';
import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async () => {
	const courses = await database.table('courses').select('name');
	return json(courses);
};
