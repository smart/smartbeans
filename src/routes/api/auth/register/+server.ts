/**
 * ### POST `/auth/register`
 *
 * Register a new user. Requires header `Authorization: Bearer <any auth.password.registration_keys>` if `auth.password.key_required = true`. Returns 200 on Success.
 *
 * ***Input***
 * ```
 * {
 *   "username": <String>,
 *   "password": <String>,
 *   "displayName?": <String>
 * }
 * ```
 *
 * ***Output***
 * ```
 * -
 * ```
 *
 * ***Errors***
 * - 400: Invalid header or body format
 * - 401: Invalid key (only with `auth.password.key_required = true`)
 * - 403: Username already exists
 *
 */
import { REGISTRATION_KEYS } from '$env/static/private';
import { createUser } from '$lib/utils/authentication/register';
import { envToKeys } from '$lib/utils/env/helper';
import { error, json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const POST: RequestHandler = async ({ request }) => {
	if (!request.body) throw error(400, 'Body must include username, password');
	const body = await request.json();
	const username = body.username;
	const password = body.password;
	let displayName = body.displayName;

	if (!username) throw error(400, 'Body must include username');
	if (!password) throw error(400, 'Body must include password');
	if (!displayName) displayName = username;

	const authHeader = request.headers.get('authorization');
	let token: string;
	if (authHeader && authHeader.startsWith('Bearer')) {
		token = authHeader.split(' ')[1];
		if (!envToKeys(REGISTRATION_KEYS).includes(token)) {
			throw error(401, 'Invalid registration key');
		}
	} else {
		throw error(401, 'Authorization header required');
	}

	const successful = await createUser(username, password, displayName);

	if (!successful) {
		throw error(400, 'Username already exists');
	}

	return json({});
};
