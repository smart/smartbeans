/**
 * ### DELETE `/api/auth/logout/<token>`
 *
 * Deletes the token from the database. Returns 200, regardless whether the token existed or not.
 *
 * ***Input/Output***
 * ```
 * -
 * ```
 */

import database from '$lib/database/database';
import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const DELETE: RequestHandler = async ({ params }) => {
	const token = params.token;
	await database.table('sessions').where('token', token).del();

	const headers = new Headers();
	headers.append('set-cookie', 'token=;path=/;');
	return json({}, { status: 200, headers });
};
