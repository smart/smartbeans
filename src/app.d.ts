// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
	namespace App {
		interface UserSession {
			username: string;
			displayName: string;
			passwordSet: boolean;
			ltiEnabled: boolean;
			activeCourse: string;
			expirationTime: number;
			avatar: Avatar;
		}
		interface Locals {
			user: UserSession | null;
			course: { name: string; title: string; config: CourseConfig } | null;
		}
	}
}

export {};
