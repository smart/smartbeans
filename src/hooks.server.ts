import { API_KEYS } from '$env/static/private';
import { courseConfiguration, userCredentials } from '$lib/session';
import { envToKeys } from '$lib/utils/env/helper';
import type { Course } from '$lib/utils/types/course';
import type { UserSession } from '$lib/utils/types/user';
import { error, type Handle } from '@sveltejs/kit';

export const handle: Handle = async ({ event, resolve }) => {
	console.log(event.url.pathname);
	const path = event.url.pathname;
	let user: UserSession | null;
	let course: Course | null;

	if (path.startsWith('/api/admin')) {
		const authHeader = event.request.headers.get('Authorization');
		if (!authHeader) {
			throw error(401, 'Unauthorized');
		}
		const authToken = authHeader.split(' ')[1];
		if (envToKeys(API_KEYS).includes(authToken)) {
			return await resolve(event);
		} else {
			throw error(401, 'Unauthorized');
		}
	} else {
		const token = event.cookies.get('token');

		try {
			if (token) {
				user = await userCredentials(token);
				course = await courseConfiguration(user.activeCourse);
			} else {
				user = null;
				course = null;
			}
		} catch {
			user = null;
			course = null;
		}

		if (!(path.startsWith('/auth') || path.startsWith('/api/auth'))) {
			if (path.startsWith('/api')) {
				if (user === null) {
					return new Response('401 Unauthorized', { status: 401 });
				}
			}
		}
	}

	event.locals.user = user;
	event.locals.course = course;

	return await resolve(event);
};
