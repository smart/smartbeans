export const envToKeys = (keys: string) => {
	return keys.split(',').map((k) => k.trim());
};
