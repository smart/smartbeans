import * as yup from 'yup';

export const schemaTopic = yup.object({
  name: yup.string().required(),
  lastTaskId: yup.number()
});

export type Topic = yup.InferType<typeof schemaTopic>;
