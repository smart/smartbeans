import { dev } from '$app/environment';
import knex, { Knex } from 'knex';
import { development, production } from '../../../knexfile';

let config: Knex.Config;
if (dev) {
	config = development;
} else {
	config = production;
}

export default knex(config);
