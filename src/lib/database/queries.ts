import database from '$lib/database/database';
import type { CourseMappingRow } from '$lib/utils/types/course';
import type { UserRow } from '$lib/utils/types/user';

export const getUserRow = (username: string): Promise<UserRow> => {
	return database
		.table('users')
		.where({
			username: username
		})
		.first() as Promise<UserRow>;
};

export const getCourseMappingRow = (studipId: string, oauth_consumer_key: string): Promise<CourseMappingRow> => {
	console.error(`we got an oauth_consumer_key: ${oauth_consumer_key}`);
	if(oauth_consumer_key) {
		return database
		.table('courseMapping')
		.where({
			studipId: studipId,
			courseName: oauth_consumer_key
		})
		.first() as Promise<CourseMappingRow>;
	}
	return database
		.table('courseMapping')
		.where({
			studipId: studipId
		})
		.first() as Promise<CourseMappingRow>;
	
};
